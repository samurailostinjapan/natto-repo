#!/bin/bash
#set -e

#Adding files to repo
#repo-add natto-repo.db.tar.gz *.pkg.tar.zst
repo-add -n -R natto-repo.db.tar.gz *.pkg.tar.zst

#Removing the symlink database files
rm natto-repo.db 
rm natto-repo.files

#Renaming the database files
mv -v natto-repo.db.tar.gz natto-repo.db
mv -v natto-repo.files.tar.gz natto-repo.files

